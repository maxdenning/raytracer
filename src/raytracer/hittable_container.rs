use super::ray::Ray;
use super::hittable::*;
use super::aabb::AABB;
use std::vec::Vec;
use std::sync::Arc;



pub struct HittableContainer {
    pub objects: Vec<Arc<dyn Hittable>>,  //TODO: should not be pub
}

impl HittableContainer {

    pub fn new() -> Self {
        Self {
            objects: Vec::new(),
        }
    }

    pub fn insert(&mut self, obj: Arc<dyn Hittable>) {
        self.objects.push(obj);
    }

    pub fn len(&self) -> usize {
        self.objects.len()
    }

}

impl Hittable for HittableContainer {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        if self.objects.is_empty() {
            return None;
        }

        let mut hit: bool = false;
        let mut closest: RayHit = Default::default();
        closest.time = t_max;

        for obj in &self.objects {
            match obj.hit(ray, t_min, closest.time) {
                Some(rh) => {
                    hit = true;
                    closest = rh;
                },
                _ => continue,
            }
        }

        if hit {
            Some(closest)
        } else {
            None
        }
    }

    fn bounding_box(&self) -> AABB {
        if self.objects.is_empty() {
            return Default::default();
        }

        let mut bounds = self.objects[0].bounding_box();
        for obj in &self.objects[1..self.len()] {
            
            bounds = AABB::surrounding(&bounds, &obj.bounding_box());
        }
        return bounds;
    }
}
