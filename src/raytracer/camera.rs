use super::ray::*;
use super::vector::*;
use super::vector_extensions::*;


#[derive(Default)]
pub struct CameraOptions {
    pub aspect_ratio: f32,
    pub look_from: Point3f,
    pub look_at: Point3f,
    pub vup: Vector3f,
    pub vfov: f32,
    pub aperture: f32,
    pub focus_dist: f32,
}

pub struct Camera {
    origin: Point3f,
    lower_left_corner: Point3f,
    horizontal: Vector3f,
    vertical: Vector3f,
    lens_radius: f32,
    u: Vector3f,
    v: Vector3f,
    w: Vector3f,
}

impl Camera {
    pub fn from(options: &CameraOptions) -> Self {
        let theta = options.vfov.to_radians();
        let h = (theta / 2.0).tan();
        let viewport_h = 2.0 * h;
        let viewport_w = options.aspect_ratio * viewport_h;

        // camera plane
        let w = (options.look_from - options.look_at).unit();
        let u = cross(&options.vup, &w).unit();
        let v = cross(&w, &u);

        let horizontal = u * options.focus_dist * viewport_w;
        let vertical = v * options.focus_dist * viewport_h;
        let lower_left_corner = options.look_from - (horizontal / 2.0) - (vertical / 2.0) - (w * options.focus_dist);

        Self {
            origin: options.look_from,
            lower_left_corner: lower_left_corner,
            horizontal: horizontal,
            vertical: vertical,
            lens_radius: options.aperture / 2.0,
            u: u,
            v: v,
            w: w,
        }
    }

    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = Vector3f::random_in_unit_disk() * self.lens_radius;
        let offset = (self.u * rd.x()) + (self.v + rd.y());
        Ray::new(
            self.origin + offset,
            self.lower_left_corner + (self.horizontal * s) + (self.vertical * t) - self.origin - offset
        )
    }
}
