use super::vector::*;
use super::vector_extensions::*;
use super::hittable::*;
use super::ray::Ray;
use super::aarect::AARect;
use super::material::MaterialRef;
use super::aabb::AABB;


pub struct Cuboid {
    min: Point3f,
    max: Point3f,
    faces: [AARect; 6],
    material: MaterialRef,
}

impl Cuboid {
    pub fn new(a: &Point3f, b: &Point3f, mat: MaterialRef) -> Self {
        
        let min = Point3f::new(a[0].min(b[0]), a[1].min(b[1]), a[2].min(b[2]));
        let max = Point3f::new(a[0].max(b[0]), a[1].max(b[1]), a[2].max(b[2]));
        let x = Point2f::new(*min.x(), *max.x());
        let y = Point2f::new(*min.y(), *max.y());
        let z = Point2f::new(*min.z(), *max.z());

        let faces: [AARect; 6] = [
            AARect::new_xy(x,        y,        *max.z(), mat.clone()),
            AARect::new_xy(x,        y,        *min.z(), mat.clone()),
            AARect::new_xz(x,        *max.y(), z,        mat.clone()),
            AARect::new_xz(x,        *min.y(), z,        mat.clone()),
            AARect::new_yz(*max.x(), y,        z,        mat.clone()),
            AARect::new_yz(*min.x(), y,        z,        mat.clone()),
        ];

        Self {
            min: min,
            max: max,
            faces: faces,
            material: mat,
        }
    }

    pub fn at(centre: &Point3f, extents: &Vector3f, mat: MaterialRef) -> Self {
        let min = centre - (extents / 2.0);
        let max = centre + (extents / 2.0);
        Self::new(&min, &max, mat)
    }
}

impl Hittable for Cuboid {
    //TODO: this is duplicate code from hittable_containers
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {        
        let mut hit: bool = false;
        let mut closest: RayHit = Default::default();
        closest.time = t_max;

        for obj in &self.faces {
            match obj.hit(ray, t_min, closest.time) {
                Some(rh) => {
                    hit = true;
                    closest = rh;
                },
                _ => continue,
            }
        }

        if hit {
            Some(closest)
        } else {
            None
        }
    }

    fn bounding_box(&self) -> AABB {
        AABB::new(&self.min, &self.max)
    }
}
