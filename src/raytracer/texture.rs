use super::colour::Colour3f;
use super::vector::*;
use super::vector_extensions::*;


pub trait Texture : Sync + Send {
    fn value(&self, u: f32, v: f32, p: &Point3f) -> &Colour3f;
}

pub type TextureRef = std::sync::Arc<dyn Texture>;

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct Solid {
    colour: Colour3f,
}

impl Solid {
    pub const fn new(colour: &Colour3f) -> Self {
        Self {
            colour: *colour,
        }
    }
}

impl Texture for Solid {
    fn value(&self, u: f32, v: f32, p: &Point3f) -> &Colour3f {
        &self.colour
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct Chequered {
    even: TextureRef,
    odd: TextureRef,
}

impl Chequered {
    pub fn new(even: TextureRef, odd: TextureRef) -> Self {
        Self {
            even: even,
            odd: odd,
        }
    }
}

impl Texture for Chequered {
    fn value(&self, u: f32, v: f32, p: &Point3f) -> &Colour3f {
        let sines = (10.0 * p.x()).sin() * (10.0 * p.y()).sin() * (10.0 * p.z()).sin();
        if sines < 0.0 {
            self.odd.value(u, v, p)
        } else {
            self.even.value(u, v, p)
        }
    }
}
