use super::vector::*;
use num::{Num, Float};
use std::ops::*;
use rand::Rng;


pub trait Vector2Ext<T> {
    fn new(x: T, y: T) -> Self;
}

pub trait Vector3Ext<T> {
    fn new(x: T, y: T, z: T) -> Self;
}

pub trait Vector3Random<T> {
    fn random_in_unit_sphere() -> Self;
    fn random_in_unit_disk() -> Self;
}

pub trait VectorFloatExt<const D: usize> {
    fn is_near_zero(&self) -> bool;
}

pub trait Vector2Getter<T> {

    #[inline]
    fn x(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(0);
    }

    #[inline]
    fn y(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(1);
    }

    #[inline]
    fn i(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(0);
    }

    #[inline]
    fn j(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(1);
    }

}

pub trait Vector3Getter<T> {

    #[inline]
    fn z(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(2);
    }

    #[inline]
    fn k(&self) -> &T
    where
        Self: Index<usize, Output = T>
    {
        return self.index(2);
    }
}

pub trait Vector4Getter<T> {

}

impl<T> Vector2Ext<T> for Vector<T, 2> {

    fn new(x: T, y: T) -> Self {
        Self::from([x, y])
    }

}

impl<T> Vector3Ext<T> for Vector<T, 3> {

    fn new(x: T, y: T, z: T) -> Self {
        Self::from([x, y, z])
    }

}

impl<T> Vector3Random<T> for Vector<T, 3>
where
    T: Num + Copy + PartialOrd + Neg<Output = T> + AddAssign + rand::distributions::uniform::SampleUniform,
    Vector3<T>: Default,
    rand::distributions::Standard: rand::distributions::Distribution<T>
{

    fn random_in_unit_disk() -> Self
    {
        // reject vectors that fall outside unit disk until success
        let mut point: Self;
        let mut rng = rand::thread_rng();
        loop {
            point = Vector3Ext::new(rng.gen_range(T::one().neg()..T::one()), rng.gen_range(T::one().neg()..T::one()), T::zero());
            if point.length_squared() < T::one() {
                return point;
            }
        }
    }

    fn random_in_unit_sphere() -> Self
    {
        // reject vectors that fall outside unit sphere until success
        loop {
            let point = Vector::random_range(T::one().neg(), T::one(), &mut rand::thread_rng());
            if point.length_squared() < T::one() {
                return point;
            }
        }
    }
}

impl<T, const D: usize> VectorFloatExt<D> for Vector<T, D>
where
    T: Float + From<f32>
{
    fn is_near_zero(&self) -> bool {
        let s: T = 1e-8.into();
        let mut near_zero: bool = true;
        for i in 0..3 {
            near_zero = near_zero && (self[i].abs() < s);
        }
        return near_zero;
    }
}

impl<T> Vector2Getter<T> for Vector<T, 2> {}

impl<T> Vector2Getter<T> for Vector<T, 3> {}
impl<T> Vector3Getter<T> for Vector<T, 3> {}

impl<T> Vector2Getter<T> for Vector<T, 4> {}
impl<T> Vector3Getter<T> for Vector<T, 4> {}
impl<T> Vector4Getter<T> for Vector<T, 4> {}


//
// utility functions
//
pub fn reflect<const D: usize>(v: &Vector<f32, D>, normal: &Vector<f32, D>) -> Vector<f32, D> {
    //TODO: dereferencing v and normal might be bad
    return *v - (*normal * (2.0 * dot(v, normal)));
}

pub fn refract<const D: usize>(uv: &Vector<f32, D>, normal: &Vector<f32, D>, etai_over_etat: f32) -> Vector<f32, D> {
    //TODO: dereferencing uv and normal might be bad
    let cos_theta = dot(&-(*uv), normal).min(1.0);
    let r_out_perp =  (*uv + (*normal * cos_theta)) * etai_over_etat;
    let r_out_parallel = normal * -(1.0 - r_out_perp.length_squared()).abs().sqrt();
    return r_out_perp + r_out_parallel;
}
