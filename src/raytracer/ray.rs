use super::vector::*;


#[derive(Default)]
pub struct Ray {
    origin: Point3f,
    direction: Vector3f,
}

impl Ray {

    pub const fn new(origin: Point3f, direction: Vector3f) -> Self {
        Self {
            origin: origin,
            direction: direction,
        }
    }

    pub fn origin(&self) -> &Point3f {
        &self.origin
    }

    pub fn direction(&self) -> &Vector3f {
        &self.direction
    }

    pub fn at(&self, time: f32) -> Point3f {
        self.origin + (self.direction * time)
    }

}
