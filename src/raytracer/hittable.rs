use super::ray::Ray;
use super::vector::*;
use super::material::Material;
use super::aabb::AABB;


#[derive(Default)]
pub struct RayHit<'a> {
    pub time: f32,
    pub point: Point3f,
    pub normal: Vector3f,
    pub is_ray_outside_surface: bool,
    pub material: Option<&'a dyn Material>,  //TODO: material isn't really optional
    pub u: f32,
    pub v: f32,
}

impl<'a> RayHit<'a> {
    pub fn new(ray: &Ray, time: f32, outward_normal: &Vector3f, material: Option<&'a dyn Material>, u: f32, v: f32) -> Self {
        let is_ray_outside_surface: bool = dot(ray.direction(), outward_normal) < 0.0;
        Self {
            time: time,
            point: ray.at(time),
            normal: if is_ray_outside_surface { *outward_normal } else { -*outward_normal },
            is_ray_outside_surface: is_ray_outside_surface,
            material: material,
            u: u,
            v: v,
        }
    }

    pub fn set_normal(&mut self, ray: &Ray, outward_normal: &Vector3f) {
        self.is_ray_outside_surface = dot(ray.direction(), outward_normal) < 0.0;
        self.normal = if self.is_ray_outside_surface { *outward_normal } else { -*outward_normal };
    }
}

pub trait Hittable : Sync + Send {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit>;
    fn bounding_box(&self) -> AABB;
}

pub type HittableRef = std::sync::Arc::<dyn Hittable>;
