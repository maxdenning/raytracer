use super::hittable::*;
use super::hittable_container::HittableContainer;
use super::aabb::AABB;
use super::ray::Ray;
use std::sync::Arc;
use std::cmp::Ordering;


pub struct BVHNode {
    left: Arc<dyn Hittable>,
    right: Arc<dyn Hittable>,
    bounds: AABB,
}

impl BVHNode {
    pub fn from(objects: &mut HittableContainer) -> Self {
        Self::build(objects.objects.as_mut_slice())
    }

    fn build(objects: &mut [Arc<dyn Hittable>]) -> Self {

        let axis = rand::random::<usize>() % 3;
        let comparator = | a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable> | -> Ordering {
            Self::compare_bounds_along_axis(a.as_ref(), b.as_ref(), axis)
        };

        let span = objects.len();

        let mut node: BVHNode = if span == 1 {
            Self {
                left: objects[0].clone(),
                right: objects[0].clone(),
                bounds: Default::default(),
            }
        } else if span == 2 {
            let o = comparator(&objects[0], &objects[1]) == Ordering::Less;
            Self {
                left: objects[if o { 0 } else { 1 }].clone(),
                right: objects[if o { 1 } else { 0 }].clone(),
                bounds: Default::default(),
            }
        } else {
            objects.sort_by(comparator);
            Self {
                left: Arc::new(Self::build(&mut objects[0..(span/2)])),
                right: Arc::new(Self::build(&mut objects[(span/2)..span])),
                bounds: Default::default(),
            }
        };

        node.bounds = AABB::surrounding(&node.left.bounding_box(), &node.right.bounding_box());
        return node;
    }

    fn compare_bounds_along_axis<'a>(a: &'a dyn Hittable, b: &'a dyn Hittable, axis: usize) -> Ordering {
        //NOTE: partial_cmp return Option, so defaults to Equal in case of NaNs etc
        //TODO: handle this properly
        a.bounding_box().min()[axis].partial_cmp(&b.bounding_box().min()[axis]).unwrap_or(Ordering::Equal)
    }
}

impl Hittable for BVHNode {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        // if this box is not it then all hittables that this node wraps are not hit
        if !self.bounds.hit(ray, t_min, t_max) {
            return None;
        }

        //TODO: simplify/clarify?
        match self.left.hit(ray, t_min, t_max) {
            Some(lhit) => {
                match self.right.hit(ray, t_min, lhit.time) {
                    Some(rhit) => Some(rhit),
                    _ => Some(lhit),
                }
            },
            _ => {
                self.right.hit(ray, t_min, t_max)
            },
        }
    }

    fn bounding_box(&self) -> AABB {
        self.bounds
    }
}
