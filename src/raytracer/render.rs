use super::ray::Ray;
use super::colour::*;
use super::hittable::Hittable;
use super::camera::Camera;
use std::io::Write;
use rand::*;
use std::sync::Arc;


pub struct RenderOptions<'a> {
    // image options
    pub image_width: usize,
    pub image_height: usize,
    pub samples_per_pixel: u16,
    pub max_depth: u8,

    // scene options
    pub camera: &'a Camera,
    pub background: &'a Colour3f,
    pub world: Arc<dyn Hittable>, //&'a dyn Hittable,
}

fn ray_colour(ray: &Ray, background: &Colour3f, world: &dyn Hittable, depth: u8) -> Colour3f {

    // at max bounce depth return black
    if depth <= 0 {
        return Default::default();
    }

    // return background colour if ray hits nothing or if there is no associated material
    let hit = world.hit(ray, 0.001, std::f32::INFINITY);
    if hit.is_none() {
        return *background;
    }

    let hit = hit.unwrap();
    if hit.material.is_none() {
        return *background;
    }

    //
    let mat = hit.material.unwrap();
    let emitted = mat.emitted(hit.u, hit.v, &hit.point);
    match mat.scatter(ray, &hit) {
        Some(scattered) => emitted + (scattered.attenuation() * ray_colour(scattered.ray(), background, world, depth - 1)),
        _ => emitted
    }
}

pub fn render_to_ppm(output: &mut dyn Write, opts: &RenderOptions) -> std::io::Result<()> {
    // seed rng
    let mut rng = rand::rngs::StdRng::seed_from_u64(0);

    // begin ppm file
    write!(output, "P3\n{} {}\n255\n", opts.image_width, opts.image_height)?;

    // render scanlines
    for y in (0..opts.image_height).rev() {
        if y % 25 == 0 {
            eprint!("\rScanlines remaining: {}", y);
        }

        for x in 0..opts.image_width {
            let mut pixel_colour: Colour3f = Default::default();

            for _ in 0..opts.samples_per_pixel {
                let u = (rng.gen_range(0.0..1.0) + x as f32) / (opts.image_width - 1) as f32;
                let v = (rng.gen_range(0.0..1.0) + y as f32) / (opts.image_height - 1) as f32;
                let ray = opts.camera.get_ray(u, v);
                pixel_colour += ray_colour(&ray, &opts.background, opts.world.as_ref(), opts.max_depth);
            }
            pixel_colour /= opts.samples_per_pixel as f32;
            write_ppm_colour(output, pixel_colour)?;
        }
    }

    Ok(())
}

/*pub fn render_to_pixels(pixels: &mut [Colour3f], opts: &RenderOptions) {
    assert_eq!(pixels.len(), opts.image_height * opts.image_width);

    // seed rng
    let mut rng = rand::rngs::StdRng::seed_from_u64(0);

    // render scanlines
    for y in 0..opts.image_height {
        if y % 25 == 0 {
            eprint!("\rScanlines remaining: {}", opts.image_height - y);
        }

        for x in 0..opts.image_width {
            let p: usize = (y * opts.image_width) + x;

            for _ in 0..opts.samples_per_pixel {
                let u = (rng.gen_range(0.0..1.0) + x as f32) / (opts.image_width - 1) as f32;
                let v = (rng.gen_range(0.0..1.0) + y as f32) / (opts.image_height - 1) as f32;
                let ray = opts.camera.get_ray(u, v);
                pixels[p] += ray_colour(&ray, &opts.background, opts.world.as_ref(), opts.max_depth);
            }
            pixels[p] /= opts.samples_per_pixel as f32;
        }
    }
}*/

pub fn render_to_pixels(pixels: &mut [Colour3f], threads: u8, opts: &RenderOptions) {
    assert_eq!(pixels.len(), opts.image_height * opts.image_width);

    let chunk_size = (opts.image_height as f32 / threads as f32).ceil() as usize * opts.image_width;
    let chunks = (pixels.len() as f32 / chunk_size as f32).ceil();

    eprintln!("Output Image: {}x{} @ {} samples/pixel ({} raycasts)", opts.image_width, opts.image_height, opts.samples_per_pixel, opts.image_height * opts.image_width * opts.samples_per_pixel as usize);
    eprintln!("Rendering {} scanlines in {} chunks ({} pixels, {} pixels/chunk)", opts.image_height, chunks, pixels.len(), chunk_size);

    let _ = crossbeam::scope(|scope| {
        for (i, chunk) in pixels.chunks_mut(chunk_size).enumerate() {
            let begin = i * chunk_size;
            let end = begin + chunk.len();
            scope.spawn(move |_scope| render_worker(chunk, &opts, begin, end));
        }
    });
}

fn render_worker(pixels: &mut [Colour3f], opts: &RenderOptions, chunk_begin: usize, chunk_end: usize) {
    // seed rng
    let mut rng = rand::rngs::StdRng::seed_from_u64(0);

    //
    let scanline_begin = chunk_begin / opts.image_width;
    let scanline_end = chunk_end / opts.image_width;
    
    //
    let thread_id = format!("{:?}", std::thread::current().id());
    eprintln!("[{}] Worker rendering chunk of {:>4} scanlines, {:>4} to {:>4} ({:>6} pixels, {:>6} to {:>6})", thread_id, scanline_end - scanline_begin, scanline_begin, scanline_end, pixels.len(), chunk_begin, chunk_end);

    // render scanlines
    for y in scanline_begin..scanline_end {
        /*if (scanline_end - y) % 100 == 0 {
            eprintln!("[{}] Scanlines remaining: {}", thread_id, scanline_end - y);
        }*/

        for x in 0..opts.image_width {
            let p: usize = ((y - scanline_begin) * opts.image_width) + x;

            for _ in 0..opts.samples_per_pixel {
                let u = (rng.gen_range(0.0..1.0) + x as f32) / (opts.image_width - 1) as f32;
                let v = (rng.gen_range(0.0..1.0) + y as f32) / (opts.image_height - 1) as f32;
                let ray = opts.camera.get_ray(u, v);
                pixels[p] += ray_colour(&ray, &opts.background, opts.world.as_ref(), opts.max_depth);
            }
            pixels[p] /= opts.samples_per_pixel as f32;
        }
    }

    eprintln!("[{}] Done.", thread_id);
}
