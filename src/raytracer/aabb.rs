use super::vector::Point3f;
use super::vector_extensions::*;
use super::ray::Ray;


#[derive(Default, Clone, Copy)]
pub struct AABB {
    min: Point3f,
    max: Point3f,
}

impl AABB {
    pub const fn new(min: &Point3f, max: &Point3f) -> Self {
        Self {
            min: *min,
            max: *max,
        }
    }

    pub fn surrounding(a: &AABB, b: &AABB) -> Self {
        Self {
            min: Point3f::new(
                a.min.x().min(*b.min.x()),
                a.min.y().min(*b.min.y()),
                a.min.z().min(*b.min.z()),
            ),
            max: Point3f::new(
                a.max.x().max(*b.max.x()),
                a.max.y().max(*b.max.y()),
                a.max.z().max(*b.max.z()),
            ),
        }
    }

    pub fn min(&self) -> &Point3f {
        &self.min
    }

    pub fn max(&self) -> &Point3f {
        &self.max
    }

    pub fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> bool {
        for a in 0..3 {
            let t0 = ((self.min[a] - ray.origin()[a]) / ray.direction()[a]).min(
                      (self.max[a] - ray.origin()[a]) / ray.direction()[a]);
            let t1 = ((self.min[a] - ray.origin()[a]) / ray.direction()[a]).max(
                      (self.max[a] - ray.origin()[a]) / ray.direction()[a]);

            let t_min = t_min.max(t0);
            let t_max = t_max.max(t1);

            if t_max <= t_min {
                return false;
            }
        }
        return true;
    }
}
