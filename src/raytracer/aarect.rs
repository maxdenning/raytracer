use super::material::MaterialRef;
use super::vector::*;
use super::vector_extensions::Vector3Ext;
use super::ray::Ray;
use super::hittable::*;
use super::aabb::AABB;


const AARECT_THICKNESS: f32 = 0.001;

pub struct AARect {
    min: Point3f,
    max: Point3f,
    axis: usize,
    material: MaterialRef
}

impl AARect {
    pub fn new_yz(x: f32, y: Point2f, z: Point2f, mat: MaterialRef) -> Self {
        Self {
            min: Point3f::new(x - AARECT_THICKNESS, y.min_component(), z.min_component()),
            max: Point3f::new(x + AARECT_THICKNESS, y.max_component(), z.max_component()),    
            axis: 0,
            material: mat,
        }
    }

    pub fn new_xz(x: Point2f, y: f32, z: Point2f, mat: MaterialRef) -> Self {
        Self {
            min: Point3f::new(x.min_component(), y - AARECT_THICKNESS, z.min_component()),
            max: Point3f::new(x.max_component(), y + AARECT_THICKNESS, z.max_component()),    
            axis: 1,
            material: mat,
        }
    }

    pub fn new_xy(x: Point2f, y: Point2f, z: f32, mat: MaterialRef) -> Self {
        Self {
            min: Point3f::new(x.min_component(), y.min_component(), z - AARECT_THICKNESS,),
            max: Point3f::new(x.max_component(), y.max_component(), z + AARECT_THICKNESS,),    
            axis: 2,
            material: mat,
        }
    }
}

impl Hittable for AARect {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {

        let t = ((self.min[self.axis] + AARECT_THICKNESS) - ray.origin()[self.axis]) / ray.direction()[self.axis];
        if t < t_min || t > t_max {
            return None;  // does not intersect within time limits
        }

        // get ray coordinates in axes at time t
        let axis_a = (self.axis + 1) % 3;
        let axis_b = (self.axis + 2) % 3;
        let a = ray.origin()[axis_a] + (t * ray.direction()[axis_a]);  // ray at time t in axis a
        let b = ray.origin()[axis_b] + (t * ray.direction()[axis_b]);  // ray at time t in axis b

        if a < self.min[axis_a] || a > self.max[axis_a] || b < self.min[axis_b] || b > self.max[axis_b] {
            return None;  // does not intersect at time t in either axis
        }

        // return hit information
        let mut outward_normal = Vector3f::zeros();
        outward_normal[self.axis] = 1.0;
        Some(RayHit::new(
            ray,
            t,
            &outward_normal,
            Some(&*self.material),
            (a - self.min[axis_a]) / (self.max[axis_a] - self.min[axis_a]),
            (b - self.min[axis_b]) / (self.max[axis_b] - self.min[axis_b]),
        ))
    }

    fn bounding_box(&self) -> AABB {
        AABB::new(&self.min, &self.max)
    }
}




