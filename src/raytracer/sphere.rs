use super::hittable::*;
use super::vector::*;
use super::vector_extensions::*;
use super::material::MaterialRef;
use super::ray::Ray;
use super::aabb::AABB;
use std::f32::consts::PI;


pub struct Sphere
{
    centre: Point3f,
    radius: f32,
    material: MaterialRef,
}

impl Sphere {
    pub fn at(centre: &Point3f, radius: f32, material: MaterialRef) -> Self {
        Self {
            centre: *centre,
            radius: radius,
            material: material,
        }
    }

    fn get_sphere_uv(p: &Point3f) -> (f32, f32) {
        // p: a given point on the sphere of radius one, centered at the origin.
        // u: returned value [0,1] of angle around the Y axis from X=-1.
        // v: returned value [0,1] of angle from Y=-1 to Y=+1.
        //     <1 0 0> yields <0.50 0.50>       <-1  0  0> yields <0.00 0.50>
        //     <0 1 0> yields <0.50 1.00>       < 0 -1  0> yields <0.50 0.00>
        //     <0 0 1> yields <0.25 0.50>       < 0  0 -1> yields <0.75 0.50>
        let theta = -p.y().acos();
        let phi = -p.z().atan2(*p.x()) + PI;
        return (phi / (2.0 * PI), theta / PI);
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        /*
        P(t) = A + tB, ray point at time t
        C = sphere centre
        (P(t) - C) ⋅ (P(t) - C) = r^2
        => (A + tB - C) ⋅ (A + tB - C) = r^2
        => t^2(B ⋅ B) + 2tB ⋅ (A - C) + (A - C) ⋅ (A - C) - r^2 = 0

        In quadratic formula form:
            a = (B ⋅ B)
            b = 2B ⋅ (A - C)
            c = (A - C) - r^2
        
        Solve for t to find when the ray passes through the sphere.
        Equation is quadratic so solve discriminant to find number of solutions.
        If discriminant == 0 then no roots,
                        == 1 then 1 root,
                        == 2 then 2 roots.
        
        Notes: * vector dotted with self = vector length ^2
            * can simplify quadratic formula to: (-h +- sqrt(h^2 - ac)) / a
        */

        let oc = ray.origin() - self.centre;
        let a = ray.direction().length_squared();
        let half_b = dot(&oc, ray.direction());
        let c = oc.length_squared() - (self.radius * self.radius);
        let discriminant = (half_b * half_b) - (a * c);

        if discriminant < 0.0 {
            return None;
        }

        let time = (-half_b - discriminant.sqrt()) / a;
        if time < t_min || time > t_max {
            let time = (-half_b + discriminant.sqrt()) / a;
            if time < t_min || time > t_max {
                return None;
            }
        }

        let outward_normal = (ray.at(time) - self.centre) / self.radius;
        let (u, v) = Sphere::get_sphere_uv(&outward_normal);
        Some(RayHit::new(ray, time, &outward_normal, Some(self.material.as_ref()), u, v))
    }

    fn bounding_box(&self) -> AABB {
        AABB::new(
            &(self.centre - Vector3f::full(&self.radius)),
            &(self.centre + Vector3f::full(&self.radius)),
        )
    }
}
