use super::vector::*;
use super::vector_extensions::*;
use super::hittable::*;
use super::ray::Ray;
use super::aabb::AABB;
use std::sync::Arc;


pub struct Translate {
    hittable: Arc<dyn Hittable>,
    displacement: Vector3f,
}

impl Translate {
    pub fn new(hittable: Arc<dyn Hittable>, displacement: Vector3f) -> Self {
        Self {
            hittable: hittable,
            displacement: displacement,
        }
    }
}

impl Hittable for Translate {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        let translated_ray = Ray::new(ray.origin() - self.displacement, *ray.direction());
        match self.hittable.hit(&translated_ray, t_min, t_max) {
            Some(mut translated_hit) => {
                //TOOD: this might be broken
                translated_hit.point += self.displacement;
                //translated_hit.set_normal(&translated_ray, &translated_hit.normal);  //TODO: is this necessary?
                Some(translated_hit)
            },
            _ => None
        }
    }

    fn bounding_box(&self) -> AABB {
        let bb = self.hittable.bounding_box();
        AABB::new(
            &(bb.min() + self.displacement),
            &(bb.max() + self.displacement),
        )
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct YRotate {
    hittable: Arc<dyn Hittable>,
    sin_theta: f32,
    cos_theta: f32,
    origin: Point3f,
}

impl YRotate {
    pub fn new(hittable: Arc<dyn Hittable>, degrees: f32, origin: &Point2f) -> Self {
        Self {
            hittable: hittable,
            sin_theta: (degrees % 360.0).to_radians().sin(),
            cos_theta: (degrees % 360.0).to_radians().cos(),
            origin: Point3f::new(origin[0], 0.0, origin[1]),
        }
    }
}

impl Hittable for YRotate {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {

        // translate and rotate ray to match hittable rotation
        let origin = *ray.origin() - self.origin;
        let origin = Point3f::new(
            (self.cos_theta * origin[0]) - (self.sin_theta * origin[2]),
            origin[1],
            (self.sin_theta * origin[0]) + (self.cos_theta * origin[2]),
        );

        let direction = Vector3f::new(
            (self.cos_theta * ray.direction()[0]) - (self.sin_theta * ray.direction()[2]),
            ray.direction()[1],
            (self.sin_theta * ray.direction()[0]) + (self.cos_theta * ray.direction()[2]),
        );

        let rotated_ray = Ray::new(origin + self.origin, direction);
        match self.hittable.hit(&rotated_ray, t_min, t_max) {
            Some(mut rotated_hit) => {

                // translate and rotate hit back
                let point = rotated_hit.point - self.origin;
                let point = Point3f::new(
                    (self.cos_theta * point[0]) + (self.sin_theta * point[2]),
                    point[1],
                    (-self.sin_theta * point[0]) + (self.cos_theta * point[2])
                );

                let outward_normal = Vector3f::new(
                    (self.cos_theta * rotated_hit.normal[0]) + (self.sin_theta * rotated_hit.normal[2]),
                    rotated_hit.normal[1],
                    (-self.sin_theta * rotated_hit.normal[0]) + (self.cos_theta * rotated_hit.normal[2]),
                );

                rotated_hit.point = point + self.origin;
                rotated_hit.set_normal(&rotated_ray, &outward_normal);

                Some(rotated_hit)
            },
            _ => None
        }
    }

    fn bounding_box(&self) -> AABB {
        let bb = self.hittable.bounding_box();
        
        let mut min = Point3f::full(&std::f32::INFINITY);
        let mut max = Point3f::full(&-std::f32::INFINITY);

        for i in 0..2 {
            for j in 0..2 {
                for k in 0..2 {
                    let x = ((i as f32) * bb.max().x()) + ((1.0 - (i as f32)) * bb.min().x());
                    let y = ((i as f32) * bb.max().y()) + ((1.0 - (j as f32)) * bb.min().y());
                    let z = ((i as f32) * bb.max().z()) + ((1.0 - (k as f32)) * bb.min().z());

                    let tester = Point3f::new(
                        (self.cos_theta * x) + (self.sin_theta * z),
                        y,
                        (-self.sin_theta * x) + (self.cos_theta * z));

                    for c in 0..3 {
                        min[c] = min[c].min(tester[c]);
                        max[c] = max[c].max(tester[c]);
                    }
                }
            }
        }

        AABB::new(&min, &max)
    }
}
