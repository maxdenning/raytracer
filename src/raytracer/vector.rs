use num::Num;
use std::ops::*;
use rand::Rng;


//
// arithmetic operator generators
//
macro_rules! arithmetic_ops {
    (+, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_ops!($lhs, $rhs, Add, add, $callback););
    (-, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_ops!($lhs, $rhs, Sub, sub, $callback););
    (*, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_ops!($lhs, $rhs, Mul, mul, $callback););
    (/, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_ops!($lhs, $rhs, Div, div, $callback););

    (+=, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_assign_ops!($lhs, $rhs, AddAssign, add_assign, $callback););
    (-=, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_assign_ops!($lhs, $rhs, SubAssign, sub_assign, $callback););
    (*=, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_assign_ops!($lhs, $rhs, MulAssign, mul_assign, $callback););
    (/=, $lhs:ty, $rhs:ty, $callback:ident) => (_impl_arithmetic_assign_ops!($lhs, $rhs, DivAssign, div_assign, $callback););
}

macro_rules! _impl_arithmetic_ops {
    ($lhs:ty, $rhs:ty, $ops_trait:ident, $ops_func:ident, $callback:ident) => {
        _impl_arithmetic_op!( $lhs,  $rhs, $ops_trait, $ops_func, $callback);
        _impl_arithmetic_op!(&$lhs,  $rhs, $ops_trait, $ops_func, $callback);
        _impl_arithmetic_op!( $lhs, &$rhs, $ops_trait, $ops_func, $callback);
        _impl_arithmetic_op!(&$lhs, &$rhs, $ops_trait, $ops_func, $callback);
    };
}

macro_rules! _impl_arithmetic_assign_ops {
    ($lhs:ty, $rhs:ty, $ops_trait:ident, $ops_func:ident, $callback:ident) => {
        _impl_arithmetic_assign_op!($lhs,  $rhs, $ops_trait, $ops_func, $callback);
        _impl_arithmetic_assign_op!($lhs, &$rhs, $ops_trait, $ops_func, $callback);
    };
}

macro_rules! _impl_arithmetic_op {
    // match lhs as reference for lifetime handling
    (&$lhs:ty, $rhs:ty, $ops_trait:ident, $ops_func:ident, $callback:ident) => {
        impl<'l, T, const D: usize> $ops_trait<$rhs> for &'l $lhs
        where
            T: Num + Copy + Default + $ops_trait,
        {
            type Output = Vector<T, D>;
            fn $ops_func(self: &'l $lhs, rhs: $rhs) -> Self::Output {
                return $callback(self, &rhs, $ops_trait::$ops_func);
            }
        }
    };

    // match all others
    ($lhs:ty, $rhs:ty, $ops_trait:ident, $ops_func:ident, $callback:ident) => {
        impl<T, const D: usize> $ops_trait<$rhs> for $lhs
        where
            T: Num + Copy + Default + $ops_trait,
        {
            type Output = Vector<T, D>;
            fn $ops_func(self: $lhs, rhs: $rhs) -> Self::Output {
                return $callback(&self, &rhs, $ops_trait::$ops_func);
            }
        }
    };
}

macro_rules! _impl_arithmetic_assign_op {
    ($lhs:ty, $rhs:ty, $ops_trait:ident, $ops_func:ident, $callback:ident) => {
        impl<T, const D: usize> $ops_trait<$rhs> for $lhs
        where
            T: Num + Copy + $ops_trait,
        {
            fn $ops_func(&mut self, rhs: $rhs) {
                $callback(self, &rhs, $ops_trait::$ops_func);
            }
        }
    };
}


//
// vector definition
//
#[derive(Clone, Copy, Debug)]
pub struct Vector<T, const D: usize>
where
    T: Sized,
{
    elements: [T; D],
}

impl<T, const D: usize> Vector<T, D> {

    pub const fn dims() -> usize {
        return D;
    }

    pub const fn from(e: [T; D]) -> Self {
        Self {
            elements: e,
        }
    }

    pub fn random<R>(rng: &mut R) -> Self
    where
        R: Rng,
        Vector<T, D>: Default,
        rand::distributions::Standard: rand::distributions::Distribution<T>
    {
        let mut v: Vector<T, D> = Default::default();
        //rand::thread_rng().fill(&mut v);
        rng.fill(&mut v);
        return v;
    }

    pub fn random_range<R>(min: T, max: T, rng: &mut R) -> Self
    where
        R: Rng,
        T: Num + Copy + PartialOrd + rand::distributions::uniform::SampleUniform,
        Vector<T, D>: Default,
        rand::distributions::Standard: rand::distributions::Distribution<T>
    {
        let mut v: Vector<T, D> = Default::default();
        //let mut rng = rand::thread_rng();

        for i in 0..D {
            v[i] = rng.gen_range(min..max);
        }
        return v;
    }

    pub fn random_unit<R>(rng: &mut R) -> Self
    where
        R: Rng,
        T: Num + Copy + Default + AddAssign + From<f32> + Into<f32>,
        Vector<T, D>: Default,
        rand::distributions::Standard: rand::distributions::Distribution<T>
    {
        Self::random(rng).unit()
    }

    pub fn zeros() -> Self
    where
        T: Num + Copy
    {
        Self {
            elements: [T::zero(); D],
        }
    }

    pub fn ones() -> Self
    where
        T: Num + Copy
    {
        Self {
            elements: [T::one(); D],
        }
    }

    pub fn full(value: &T) -> Self
    where
        T: Num + Copy
    {
        Self {
            elements: [*value; D],
        }
    }

    pub fn length(&self) -> T
    where
        T: Num + std::ops::AddAssign + Copy + Into<f32> + From<f32>
    {
        return self.length_squared().into().sqrt().into();
    }

    pub fn length_squared(&self) -> T
    where
        T: Num + std::ops::AddAssign + Copy
    {
        let mut result: T = T::zero();
        for i in 0..D {
            result += self[i] * self[i];
        }
        return result;
    }

    pub fn unit(&self) -> Vector<T, D>
    where
        T: Num + Copy + Default + std::ops::AddAssign + Into<f32> + From<f32>
    {
        return self / self.length();
    }

    pub fn apply(&mut self, op: fn(T) -> T)
    where
        T: Copy
    {
        for i in 0..D {
            self[i] = op(*self.elements.index(i));
        }
    }

    pub fn mutate(&mut self, op: fn(&mut T)) {
        for i in 0..D {
            op(self.elements.index_mut(i));
        }
    }

    pub fn map(&self, op: fn(T) -> T) -> Vector<T, D>
    where
        T: Copy,
        Vector<T, D>: Default
    {
        let mut result: Vector<T, D> = Default::default();
        for i in 0..D {
            result[i] = op(self[i]);
        }
        return result;
    }

    pub fn min_component(&self) -> T
    where
    T: Copy + PartialOrd
    {
        let mut min = self[0];
        for i in 1..D {
            if self[i] < min {
                min = self[i]
            }
        }
        return min;
    }

    pub fn max_component(&self) -> T
    where
        T: Copy + PartialOrd
    {
        let mut max = self[0];
        for i in 1..D {
            if self.elements[i] > max {
                max = self[i]
            }
        }
        return max;
    }
}


//
// misc traits
//
impl<T, const D: usize> Default for Vector<T, D>
where
    T: Copy + Default,
{
    fn default() -> Self {
        //Self::zeroes()
        Self {
            elements: [Default::default(); D],
        }
    }
}

/*impl<T, const D: usize> std::fmt::Display for Vector<T, D>
where
    T: std::fmt::Display
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        //TODO: this is bad code
        write!(f, "(");
        for i in 0..D {
            write!(f, "{}", self[i]);
            if i != (D - 1) {
                write!(f, ", ");
            }
        }
        return write!(f, ")");
    }
}*/

impl<T, const D: usize> std::ops::Neg for Vector<T, D>
where
    T: Copy + Default + std::ops::Neg<Output = T>
{
    type Output = Vector<T, D>;

    #[inline]
    fn neg(self) -> Self::Output {
        return self.map(std::ops::Neg::neg);
    }
}

impl<T, const D: usize> rand::Fill for Vector<T, D>
where
    rand::distributions::Standard: rand::distributions::Distribution<T>
{
    fn try_fill<R: rand::Rng + ?Sized>(&mut self, rng: &mut R) -> Result<(), rand::Error> {
        for i in 0..D {
            self[i] = rng.gen();
        }
        Ok(())
    }
}


//
// index operators
//
impl<T, const D: usize> std::ops::Index<usize> for Vector<T, D> {
    type Output = T;

    #[inline]
    fn index(&self, index: usize) -> &T {
        return self.elements.index(index);
    }
}

impl<T, const D: usize> std::ops::IndexMut<usize> for Vector<T, D> {
    #[inline]
    fn index_mut(&mut self, index: usize) -> &mut T {
        return self.elements.index_mut(index);
    }
}


//
// arithmetic operators
//
arithmetic_ops!(+, Vector<T, D>, Vector<T, D>, vector_element_op);
arithmetic_ops!(-, Vector<T, D>, Vector<T, D>, vector_element_op);
arithmetic_ops!(*, Vector<T, D>, Vector<T, D>, vector_element_op);
arithmetic_ops!(/, Vector<T, D>, Vector<T, D>, vector_element_op);

arithmetic_ops!(+, Vector<T, D>, T, scalar_element_op);
arithmetic_ops!(-, Vector<T, D>, T, scalar_element_op);
arithmetic_ops!(*, Vector<T, D>, T, scalar_element_op);
arithmetic_ops!(/, Vector<T, D>, T, scalar_element_op);

arithmetic_ops!(+=, Vector<T, D>, Vector<T, D>, vector_element_assign_op);
arithmetic_ops!(-=, Vector<T, D>, Vector<T, D>, vector_element_assign_op);
arithmetic_ops!(*=, Vector<T, D>, Vector<T, D>, vector_element_assign_op);
arithmetic_ops!(/=, Vector<T, D>, Vector<T, D>, vector_element_assign_op);

arithmetic_ops!(+=, Vector<T, D>, T, scalar_element_assign_op);
arithmetic_ops!(-=, Vector<T, D>, T, scalar_element_assign_op);
arithmetic_ops!(*=, Vector<T, D>, T, scalar_element_assign_op);
arithmetic_ops!(/=, Vector<T, D>, T, scalar_element_assign_op);


//
// comparators
//
impl<T, const D: usize> std::cmp::PartialEq for Vector<T, D>
where
    T: PartialEq
{
    fn eq(&self, other: &Self) -> bool {
        let mut result: bool = true;
        for i in 0..D {
            result = result && self[i] == other[i];
        }
        return result;
    }
}



//
// vector operation helpers
//
fn vector_element_op<T, const D: usize>(lhs: &Vector<T, D>, rhs: &Vector<T, D>, op: fn(T, T) -> T) -> Vector<T, D>
where
    T: Copy + Default,
{
    let mut result: Vector<T, D> = Default::default();
    for i in 0..D {
        result[i] = op(lhs[i], rhs[i]);
    }
    return result;
}

fn vector_element_assign_op<T, const D: usize>(lhs: &mut Vector<T, D>, rhs: &Vector<T, D>, op: fn(&mut T, T))
where
    T: Copy,
{
    for i in 0..D {
        op(&mut lhs[i], rhs[i]);
    }
}

fn scalar_element_op<T, const D: usize>(lhs: &Vector<T, D>, rhs: &T, op: fn(T, T) -> T) -> Vector<T, D>
where
    T: Copy + Default,
{
    let mut result: Vector<T, D> = Default::default();
    for i in 0..D {
        result[i] = op(lhs[i], *rhs);
    }
    return result;
}

fn scalar_element_assign_op<T, const D: usize>(lhs: &mut Vector<T, D>, rhs: &T, op: fn(&mut T, T))
where
    T: Copy,
{
    for i in 0..D {
        op(&mut lhs[i], *rhs);
    }
}


//
// utility functions
//
pub fn dot<T, const D: usize>(u: &Vector<T, D>, v: &Vector<T, D>) -> T
where
    T: Num + Copy + std::ops::AddAssign,
{
    let mut out: T = T::zero();
    for i in 0..D {
        out += u[i] * v[i];
    }
    return out;
}

pub fn cross<T>(u: &Vector<T, 3>, v: &Vector<T, 3>) -> Vector<T, 3>
where
    T: Num + Copy
{
    Vector::from([
        (u[1] * v[2]) - (u[2] * v[1]),
        (u[2] * v[0]) - (u[0] * v[2]),
        (u[0] * v[1]) - (u[1] * v[0]),
    ])
}


//
// type alias
//
pub type Vector2<T> = Vector<T, 2>;
pub type Vector3<T> = Vector<T, 3>;
pub type Vector2f = Vector2<f32>;
pub type Vector3f = Vector3<f32>;

pub type Point2<T> = Vector<T, 2>;
pub type Point3<T> = Vector<T, 3>;
pub type Point2f = Point2<f32>;
pub type Point3f = Point3<f32>;
