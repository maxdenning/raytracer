use super::colour::Colour3f;
use super::vector::*;
use super::vector_extensions::*;
use super::ray::Ray;
use super::hittable::*;
use super::texture::TextureRef;


#[derive(Default)]
pub struct ScatteredRay {
    attenuation: Colour3f,
    ray: Ray,
}

impl ScatteredRay {
    pub const fn new(attenuation: Colour3f, ray: Ray) -> Self {
        Self {
            attenuation: attenuation,
            ray: ray,
        }
    }

    pub fn attenuation(&self) -> &Colour3f {
        &self.attenuation
    }

    pub fn ray(&self) -> &Ray {
        &self.ray
    }
}


pub trait Material : Sync + Send {
    fn scatter(&self, ray: &Ray, hit: &RayHit) -> Option<ScatteredRay>;

    fn emitted(&self, u: f32, v: f32, p: &Point3f) -> Colour3f {
        Default::default()
    }
}

pub type MaterialRef = std::sync::Arc<dyn Material>;

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct Lambertian {
    albedo: TextureRef,
}

impl Lambertian {
    pub fn new(albedo: TextureRef) -> Self {
        Self {
            albedo: albedo,
        }
    }
}

impl Material for Lambertian {
    fn scatter(&self, ray: &Ray, hit: &RayHit) -> Option<ScatteredRay> {
        let mut scatter_direction = hit.normal + Vector3f::random_unit(&mut rand::thread_rng());
        if scatter_direction.is_near_zero() {
            scatter_direction = hit.normal;  // avoid scatter direction of zero
        }

        Some(ScatteredRay::new(
            *self.albedo.value(hit.u, hit.v, &hit.point),
            Ray::new(hit.point, scatter_direction),
        ))
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct Metal {
    albedo: TextureRef,
    fuzz: f32,
}

impl Metal {
    pub fn new(albedo: TextureRef, fuzz: f32) -> Self {
        Self {
            albedo: albedo,
            fuzz: fuzz.min(1.0),
        }
    }
}

impl Material for Metal {
    fn scatter(&self, ray: &Ray, hit: &RayHit) -> Option<ScatteredRay> {
        let reflected = reflect(&ray.direction().unit(), &hit.normal);
        let scatter_direction = reflected + (Vector3f::random_in_unit_sphere() * self.fuzz);

        //BUG: if face is at ~45degrees to camera then this is never true
        if dot(&scatter_direction, &hit.normal) > 0.0 {
            Some(ScatteredRay::new(
                *self.albedo.value(hit.u, hit.v, &hit.point),
                Ray::new(hit.point, scatter_direction),
            ))
        } else {
            None
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct Dielectric {
    refraction_index: f32,
}

impl Dielectric {
    pub fn new(refraction_index: f32) -> Self {
        Self {
            refraction_index: refraction_index,
        }
    }

    fn reflectance(cosine: f32, ref_idx: f32) -> f32 {
        // Schlick's approximation for reflectance
        let mut r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
        r0 = r0 * r0;
        return r0 + (1.0 - r0) * (1.0 - cosine).powf(5.0);
    }
}

impl Material for Dielectric {
    fn scatter(&self, ray: &Ray, hit: &RayHit) -> Option<ScatteredRay> {

        let refraction_ratio = if hit.is_ray_outside_surface {
            1.0 / self.refraction_index
        } else {
            self.refraction_index
        };

        let unit_direction = ray.direction().unit();
        let cos_theta = dot(&-unit_direction, &hit.normal).min(1.0);
        let sin_theta = (1.0 - (cos_theta * cos_theta)).sqrt();
        let cannot_refract = (refraction_ratio * sin_theta) > 1.0;

        let direction = if cannot_refract || (Dielectric::reflectance(cos_theta, refraction_ratio) > rand::random()) {
            // cannot refract or is reflected
            reflect(&unit_direction, &hit.normal)
        } else {
            refract(&unit_direction, &hit.normal, refraction_ratio)
        };

        Some(ScatteredRay::new(
            Colour3f::ones(),
            Ray::new(hit.point, direction),
        ))
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

pub struct DiffuseLight {
    albedo: TextureRef,
}

impl DiffuseLight {
    pub fn new(albedo: TextureRef) -> Self {
        Self {
            albedo: albedo,
        }
    }
}

impl Material for DiffuseLight {
    fn scatter(&self, ray: &Ray, hit: &RayHit) -> Option<ScatteredRay> {
        None
    }

    fn emitted(&self, u: f32, v: f32, p: &Point3f) -> Colour3f {
        *self.albedo.value(u, v, p)
    }
}
