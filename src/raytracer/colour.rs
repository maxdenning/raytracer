use super::vector::*;
use super::vector_extensions::*;


pub type Colour3<T> = Vector3<T>;
pub type Colour3f = Vector3f;  // 3 channels in 0 to 1 range
pub type Colour3u = Vector3<u8>;  // 3 channels in 0 to 255 range

trait Colour3Getter<T> {
    #[inline]
    fn r(&self) -> &T
    where
        Self: std::ops::Index<usize, Output = T>
    {
        return self.index(0);
    }

    #[inline]
    fn g(&self) -> &T
    where
        Self: std::ops::Index<usize, Output = T>
    {
        return self.index(1);
    }

    #[inline]
    fn b(&self) -> &T
    where
        Self: std::ops::Index<usize, Output = T>
    {
        return self.index(2);
    }
}

impl<T> Colour3Getter<T> for Colour3<T> {}

pub fn colourf_to_colouru(colour: &Colour3f) -> Colour3u {
    Colour3u::new(
        (256.0 * colour.r().clamp(0.0, 0.999)) as u8,
        (256.0 * colour.g().clamp(0.0, 0.999)) as u8,
        (256.0 * colour.b().clamp(0.0, 0.999)) as u8,
    )
}

pub fn write_ppm_colour(out: &mut dyn std::io::Write, mut colour: Colour3f) -> std::io::Result<()> {

    colour.apply(| v | if v.is_nan() { 0.0 } else { v });  // handle NaNs to avoid noise
    colour.apply(f32::sqrt);  // gamma correction with gamma = 2.0

    let col_ppm: Colour3u = colourf_to_colouru(&colour);
    write!(out, "{} {} {}\n", col_ppm.r(), col_ppm.g(), col_ppm.b())?;
    Ok(())
}

pub fn write_ppm(out: &mut dyn std::io::Write, imwidth: usize, imheight: usize, pixels: &[Colour3f]) -> std::io::Result<()> {
    // write ppm headers
    write!(out, "P3\n{} {}\n255\n", imwidth, imheight)?;

    // write pixel values
    for y in (0..imheight).rev() {
        for x in 0..imwidth {
            write_ppm_colour(out, pixels[(y * imwidth) + x])?;
        }
    }

    Ok(())
}
