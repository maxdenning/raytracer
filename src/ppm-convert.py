import cv2
import sys
import os

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(f"{sys.argv[0]} <input file> <output file>")
        exit(1)

    flags = []
    if os.path.splitext(sys.argv[2])[1] == ".jpg":
        flags = [cv2.IMWRITE_JPEG_QUALITY, 90]

    cv2.imwrite(sys.argv[2], cv2.imread(sys.argv[1]), flags)
