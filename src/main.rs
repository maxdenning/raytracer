mod raytracer;

use raytracer::vector::*;
use raytracer::vector_extensions::*;
use raytracer::camera::*;
use raytracer::colour::*;
use raytracer::hittable::*;
use raytracer::hittable_container::*;
use raytracer::material::*;
use raytracer::texture::*;
use raytracer::sphere::*;
use raytracer::render::*;
use raytracer::cuboid::*;
use raytracer::aarect::*;
use raytracer::transform::*;
use raytracer::bvh::*;
use std::io::stdout;
use std::sync::Arc;
use rand::*;
use std::io;


fn random_scene() -> HittableContainer {
    let mut world = HittableContainer::new();

    let dgreen = Arc::new(Solid::new(&Colour3f::new(0.2, 0.3, 0.1)));
    let dgrey  = Arc::new(Solid::new(&Colour3f::new(0.1, 0.1, 0.08)));
    let cheq   = Arc::new(Chequered::new(dgreen, dgrey));
    let mgrey  = Arc::new(Solid::new(&Colour3f::new(0.7, 0.6, 0.5)));
    let dbrown = Arc::new(Solid::new(&Colour3f::new(0.4, 0.2, 0.1)));
    let light  = Arc::new(Solid::new(&Colour3f::new(2.0, 2.0, 2.0)));

    let s0mat = Arc::new(Lambertian::new(cheq));  
    let s1mat = Arc::new(Lambertian::new(dbrown));
    let s2mat = Arc::new(Dielectric::new(1.5));
    let s3mat = Arc::new(Metal::new(mgrey, 0.0));
    let s4mat = Arc::new(DiffuseLight::new(light));

    let sphere0 = Arc::new(Sphere::at(&Point3f::new( 0.0, -1000.0, 0.0), 1000.0, s0mat.clone()));
    let sphere1 = Arc::new(Sphere::at(&Point3f::new(-4.0, 1.0, 0.0), 1.0, s1mat.clone()));
    let sphere2 = Arc::new(Sphere::at(&Point3f::new( 0.0, 1.0, 0.0), 1.0, s2mat.clone()));
    let sphere3 = Arc::new(Sphere::at(&Point3f::new( 4.0, 1.0, 0.0), 1.0, s3mat.clone()));
    let sphere4 = Arc::new(Sphere::at(&Point3f::new( 0.0, 2.5, 4.0), 0.75, s4mat.clone()));

    let cuboid1 = Arc::new(Cuboid::at(&Point3f::new(-4.0, 1.5, -2.5), &Point3f::ones(), s1mat.clone()));
    let cuboid2 = Arc::new(Cuboid::at(&Point3f::new( 0.0, 1.5, -2.5), &Point3f::ones(), s2mat.clone()));
    let cuboid3 = Arc::new(Cuboid::at(&Point3f::new( 4.0, 1.5, -2.5), &Point3f::ones(), s3mat.clone()));

//    let sphere1 = Rc::new(YRotate::new(sphere1, 45.0, &Point2f::new(-4.0, 0.0)));
//    let sphere2 = Rc::new(YRotate::new(sphere2, 45.0, &Point2f::new( 0.0, 0.0)));
//    let sphere3 = Rc::new(YRotate::new(sphere3, 45.0, &Point2f::new( 4.0, 0.0)));
//    let cuboid1 = Rc::new(YRotate::new(cuboid1, 45.0, &Point2f::new(-4.0, -2.5)));
//    let cuboid2 = Rc::new(YRotate::new(cuboid2, 45.0, &Point2f::new( 0.0, -2.5)));
//    let cuboid3 = Rc::new(YRotate::new(cuboid3, 45.0, &Point2f::new( 4.0, -2.5)));

    world.insert(sphere0);
    world.insert(sphere1);
    world.insert(sphere2);
    world.insert(sphere3);
    world.insert(sphere4);

    world.insert(cuboid1);
    world.insert(cuboid2);
    world.insert(cuboid3);

    // randomly populate scene
    let mut rng = rand::rngs::StdRng::seed_from_u64(0);
    for a in (-17..17).step_by(2) {
        for b in (-7..7).step_by(2) {
            // 12.5% chance of skipping each shape
            if rng.gen_range(0.0..1.0) < 0.125 {
                continue;
            }

            // generate matierial
            let choose_mat: f32 = rng.gen_range(0.0..1.0);
            let material: MaterialRef =
                if choose_mat < 0.15 {
                    // dielectric (glass) 15%
                    Arc::new(Dielectric::new(1.5))
                } else {
                    let choose_tex: f32 = rng.gen_range(0.0..1.0);
                    let texture: TextureRef =
                        if choose_tex < 0.75 {
                            // solid colour 75%
                            Arc::new(Solid::new(&Colour3f::random(&mut rng)))
                        } else {
                            // chequered 20%
                            let cheq_even = Arc::new(Solid::new(&Colour3f::random(&mut rng)));
                            let cheq_odd = Arc::new(Solid::new(&Colour3f::random(&mut rng)));
                            Arc::new(Chequered::new(cheq_even, cheq_odd))
                        };
                    
                    if choose_mat < 0.65 {
                        // lambertian (diffuse) 50%
                        Arc::new(Lambertian::new(texture))
                    } else {
                        // metal 35%
                        Arc::new(Metal::new(texture, rng.gen_range(0.0..0.2)))
                    }
                };
            
            // generate location
            let radius = rng.gen_range(0.2..0.45);
            let dist = 1.35;
            let centre: Point3f = loop {
                let position = Point3f::new(a as f32 + (dist * rng.gen_range(0.0..1.0)), radius, b as f32 + (dist * rng.gen_range(0.0..1.0)));
                if (position - Point3f::new(4.0, radius, 0.0)).length() > dist {
                    break position;
                }
            };

            // generate shape
            let choose_shape = rng.gen_range(0.0..1.0);
            let shape: HittableRef =
                if choose_shape < 0.75 {
                    // sphere
                    Arc::new(Sphere::at(&centre, radius, material))

                } else {
                    // cuboid
                    Arc::new(Cuboid::at(&centre, &Point3f::full(&radius), material))
                };
            world.insert(shape);
        }
    }

    return world;
}

fn cornell_box_scene() -> HittableContainer {
    let mut world = HittableContainer::new();

    let red   = Arc::new(Solid::new(&Colour3f::new(0.65, 0.05, 0.05)));
    let white = Arc::new(Solid::new(&Colour3f::new(0.73, 0.73, 0.73)));
    let green = Arc::new(Solid::new(&Colour3f::new(0.12, 0.45, 0.15)));
    let light = Arc::new(Solid::new(&Colour3f::new(1.0, 1.0, 1.0)));

    let mat_red   = Arc::new(Lambertian::new(red.clone()));
    let mat_white = Arc::new(Lambertian::new(white.clone()));
    let mat_green = Arc::new(Lambertian::new(green.clone()));
    let mat_light = Arc::new(DiffuseLight::new(light.clone()));

    let green_wall  = Arc::new(AARect::new_yz(5.550,                      Point2f::new(0.0, 5.550), Point2f::new(0.0,   5.550), mat_green.clone()));
    let red_wall    = Arc::new(AARect::new_yz(0.0,                        Point2f::new(0.0, 5.550), Point2f::new(0.0,   5.550), mat_red.clone()));
    let light_wall  = Arc::new(AARect::new_xz(Point2f::new(2.130, 3.430), 5.540,                    Point2f::new(2.270, 3.320), mat_light.clone()));
    let white_wall0 = Arc::new(AARect::new_xz(Point2f::new(0.0,   5.550), 5.550,                    Point2f::new(0.0, 5.550),   mat_white.clone()));
    let white_wall1 = Arc::new(AARect::new_xz(Point2f::new(0.0,   5.550), 0.0,                      Point2f::new(0.0, 5.550),   mat_white.clone()));
    let white_wall2 = Arc::new(AARect::new_xy(Point2f::new(0.0,   5.550), Point2f::new(0.0, 5.550), 5.550,                      mat_white.clone()));

    let box1 = Arc::new(Cuboid::new(&Point3f::new(0.0, 0.0, 0.0), &Point3f::new(1.65, 3.30, 1.65), mat_white.clone()));
    let box1 = Arc::new(YRotate::new(box1, 15.0, &Point2f::zeros()));
    let box1 = Arc::new(Translate::new(box1, Vector3f::new(2.65, 0.0, 2.95)));

    let box2 = Arc::new(Cuboid::new(&Point3f::new(0.0, 0.0, 0.0), &Point3f::new(1.65, 1.65, 1.65), mat_white.clone()));
    let box2 = Arc::new(YRotate::new(box2, -18.0, &Point2f::zeros()));
    let box2 = Arc::new(Translate::new(box2, Vector3f::new(1.30, 0.0, 0.65)));

    world.insert(green_wall);
    world.insert(red_wall);
    world.insert(light_wall);
    world.insert(white_wall0);
    world.insert(white_wall1);
    world.insert(white_wall2);
    world.insert(box1);
    world.insert(box2);

    return world;
}

fn main() -> Result<(), std::io::Error> {
    // get input args
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 4 {
        eprintln!("help:  {} <scene_name (e.g. random or cornell)> <image_width> <samples_per_pixel>", args[0]);
        return Err(io::Error::from(io::ErrorKind::InvalidInput));
    }

    // scene select
    let mut camera_opts: CameraOptions = Default::default();
    let mut background: Colour3f = Default::default();
    let mut world: HittableContainer = HittableContainer::new();

    match args[1].as_str() {
        "random" => {
            // scene
            background = Colour3f::new(0.3, 0.42, 0.6);
            world = random_scene();

            // camera
            camera_opts = CameraOptions {
                aspect_ratio: 16.0 / 9.0,
                look_from: Point3f::new(13.0, 2.0, 3.0),
                look_at: Point3f::new(0.0, 0.0, 0.0),
                vup: Vector3f::new(0.0, 1.0, 0.0),
                vfov: 25.0,
                focus_dist: 10.0,
                aperture: 0.02,
            };
        },
        "cornell" => {
            // scene
            background = Colour3f::new(0.4, 0.4, 0.4);
            world = cornell_box_scene();

            // camera
            camera_opts = CameraOptions {
                aspect_ratio: 1.0,
                look_from: Point3f::new(2.78, 2.73, -8.00),
                look_at: Point3f::new(2.78, 2.73, 0.0),
                vup: Vector3f::new(0.0, 1.0, 0.0),
                vfov: 40.0,
                focus_dist: 10.0,
                aperture: 0.01,
            };
        },
        _ => {
            eprintln!("Scene name was not recognised");
            return Err(io::Error::from(io::ErrorKind::InvalidInput));
        },
    }

    // convert world to volume hierarchy
    let world = BVHNode::from(&mut world);

    // image options
    let camera = Camera::from(&camera_opts);
    let image_width: u16 = args[2].parse().expect("Failed to parse image width argument"); //1200_u16;
    let image_height = (image_width as f32 / camera_opts.aspect_ratio) as u16;
    let samples_per_pixel = args[3].parse().expect("Failed to parse samples per pixel argument"); //10_u16;
    let max_depth = 255;

    // render options
    let opts = RenderOptions {
        image_width: image_width as usize,
        image_height: image_height as usize,
        samples_per_pixel: samples_per_pixel,
        max_depth: max_depth,

        camera: &camera,
        background: &background,
        world: std::sync::Arc::new(world),
    };

    // reserve memory
    let mut pixels = vec![Colour3f::zeros(); opts.image_height * opts.image_width];
    eprintln!("Reserved: {}MB", (std::mem::size_of::<Colour3f>() * opts.image_height * opts.image_width) / (1024 * 1024));

    // start timer
    let start = std::time::Instant::now();

    // render
    render_to_pixels(&mut pixels, 4, &opts);
    write_ppm(&mut stdout(), opts.image_width, opts.image_height, &pixels)?;

    // end timer
    eprint!("\nDone. {}s\n", start.elapsed().as_millis() as f32 / 1000.0);
    Ok(())
}
