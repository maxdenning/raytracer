A simple ray tracer written in rust.

Based on the [Ray Tracing In One Weekend](https://raytracing.github.io/) book series.

![](/examples/example4k.jpg)
